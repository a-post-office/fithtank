# Fithtank #

A language-enhancing userscript promoting an absurdist dialect of Newspeak (stolen from 'Mr Toad,' a fork of 'Cloud-to-Butt')

### Usage ###

This script is for use with Grease/Tamper/Violent/etcMonkey browser extensions. Install one from one of these webstore links:

 * [TamperMonkey for Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
 * [ViolentMonkey for Opera](https://addons.opera.com/en/extensions/details/violent-monkey/)
 * [GreaseMonkey for Firefox](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)

 ...or check your own browser's webstore for more

 If you already have a userscript manager installed, click [here](https://bitbucket.org/a-post-office/fithtank/raw/master/fithtank.user.js) to install the script.

### Contact ###

Email me at apostoffice[at]tfwno.gf
Sometimes I'm on [Mastodon](https://cybre.space/@apostoffice)
